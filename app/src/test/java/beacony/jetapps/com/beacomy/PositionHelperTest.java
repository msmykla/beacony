package beacony.jetapps.com.beacomy;

import org.junit.Test;

import beacony.jetapps.com.beacomy.models.MeasurePoint;
import beacony.jetapps.com.beacomy.models.PositionModel;
import beacony.jetapps.com.beacomy.utils.PositionHelper;

import static org.junit.Assert.*;

/**
 * Created by smykala-pc on 30/01/2018.
 */
public class PositionHelperTest {

    public static final String TAG = PositionHelperTest.class.getSimpleName();

    @Test
    public void getPosition() throws Exception {
        MeasurePoint p1=new MeasurePoint(new PositionModel(1,0), 3.6);
        MeasurePoint p2=new MeasurePoint(new PositionModel(3,5), 3);
        MeasurePoint p3=new MeasurePoint(new PositionModel(7,0), 3.6);

        double[] expect = new double[]{4.0,2.0};
        double delta = 0.2;

        double[] result = PositionHelper.getPosition(p1,p2,p3);

        assertArrayEquals(expect,result,delta);

        p1.getPosition().setX(0);
        p1.getPosition().setY(0);
        p1.setDistance(4.47);
        p2.getPosition().setX(0);
        p2.getPosition().setY(5);
        p2.setDistance(5);
        p3.getPosition().setX(5);
        p3.getPosition().setY(0);
        p3.setDistance(2.24);

        result = PositionHelper.getPosition(p1,p2,p3);

        assertArrayEquals(expect,result,delta);
    }

}