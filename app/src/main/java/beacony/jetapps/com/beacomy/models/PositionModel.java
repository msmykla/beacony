package beacony.jetapps.com.beacomy.models;

/**
 * Created by smykala-pc on 30/01/2018.
 */

public class PositionModel {

    private double x = 0;
    private double y = 0;

    public PositionModel() {
    }

    public PositionModel(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

}
