package beacony.jetapps.com.beacomy.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.estimote.coresdk.common.requirements.SystemRequirementsChecker;
import com.estimote.coresdk.observation.region.beacon.BeaconRegion;
import com.estimote.coresdk.recognition.packets.Beacon;
import com.estimote.coresdk.service.BeaconManager;

import java.util.ArrayList;
import java.util.List;

import beacony.jetapps.com.beacomy.R;
import beacony.jetapps.com.beacomy.models.AccessPoint;
import beacony.jetapps.com.beacomy.models.BeaconModel;
import beacony.jetapps.com.beacomy.utils.Constants;
import beacony.jetapps.com.beacomy.utils.OnWifiListener;
import beacony.jetapps.com.beacomy.utils.PositionHelper;
import beacony.jetapps.com.beacomy.utils.WifiScanner;
import beacony.jetapps.com.beacomy.utils.custom_views.MapView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MapActivity extends AppCompatActivity implements BeaconManager.BeaconRangingListener, OnWifiListener {

    public static final String TAG = MapActivity.class.getSimpleName();
    @BindView(R.id.map)
    MapView mapView;
    private BeaconManager beaconManager;
    private List<BeaconModel> listBM = new ArrayList<>();
    private List<AccessPoint> listAP = new ArrayList<>();
    private WifiScanner wifiScanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        ButterKnife.bind(this);
        loadBeaconsData();
        loadAccessPointsData();
        initBeacons();
        initViews();
        initWifiScanner();
    }


    private void loadAccessPointsData() {

    }

    private void initWifiScanner() {
        wifiScanner = new WifiScanner(getApplicationContext(), this);
    }


    private void loadBeaconsData() {
        BeaconModel bm1 = new BeaconModel(Constants.UUID_BLUE, Constants.beacons[0][0], Constants.beacons[0][1]);
        BeaconModel bm2 = new BeaconModel(Constants.UUID_GREEN, Constants.beacons[1][0], Constants.beacons[1][1]);
        BeaconModel bm3 = new BeaconModel(Constants.UUID_BLUEBERRY, Constants.beacons[2][0], Constants.beacons[2][1]);
        listBM.add(bm1);
        listBM.add(bm2);
        listBM.add(bm3);
    }

    private void initViews() {
        mapView.calculate(this, Constants.roomWidth, Constants.roomHeight, Constants.cellSize, Constants.map);
        mapView.setBecaons(listBM);
    }

    private void initBeacons() {
        beaconManager = new BeaconManager(getApplicationContext());
        beaconManager.setRangingListener(this);
        beaconManager.setForegroundScanPeriod(1, 0);
        beaconManager.setBackgroundScanPeriod(1, 0);
        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                Log.d(TAG, "onServiceReady() called");
                beaconManager.startRanging(Constants.ALL_ESTIMOTE_BEACONS);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        SystemRequirementsChecker.checkWithDefaultDialogs(this);
        wifiScanner.register(getApplicationContext());
    }

    @Override
    protected void onPause() {
        super.onPause();
        wifiScanner.unregister(getApplicationContext());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        beaconManager.stopRanging(Constants.ALL_ESTIMOTE_BEACONS);
        beaconManager.disconnect();
    }

    @Override
    public void onBeaconsDiscovered(BeaconRegion beaconRegion, List<Beacon> beacons) {
        for (Beacon b : beacons) {
            int pos = listBM.indexOf(b.getProximityUUID());
            if (pos >= 0) {
                listBM.get(pos).setBeacon(b);
            }
        }
        mapView.calculatePosition();
    }

    @Override
    public void onWifiScanned(List<AccessPoint> accessPoints) {
        for (AccessPoint ap : accessPoints) {
            Log.d(TAG, "onWifiScanned() called : ssid = " + ap.getSsid() + ", d = " + PositionHelper.getDistance(ap));
        }
//        for(AccessPoint ap: accessPoints){
//            int pos = listAP.indexOf(ap.getBssid());
//            if(pos>=0) {
//                listAP.get(pos).setRssi(ap.getRssi());
//            }
//        }
//        mapView.calculatePosition();
    }
}
