package beacony.jetapps.com.beacomy.utils.custom_views;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.List;

import beacony.jetapps.com.beacomy.R;
import beacony.jetapps.com.beacomy.models.AccessPoint;
import beacony.jetapps.com.beacomy.models.BeaconModel;

/**
 * Created by MaciekS on 2017-01-07.
 */

public class MapView extends RelativeLayout {

    public static final String TAG = MapView.class.getSimpleName();
    private int width;
    private int height;
    private int room_width;
    private int room_height;
    private int one_m;
    private int step;
    private int startX;
    private int startY;
    private Bitmap beacon_icon;
    private Bitmap user_icon;
    private float dp;
    private Paint paint;
    private Path path;
    private float[][] map;
    private List<BeaconModel> beacons;
    private List<AccessPoint> accessPoints;
    private ImageView imageView;


    public MapView(Context context) {
        super(context);
        init(context);
    }

    public MapView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public MapView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public MapView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context) {
        paint = new Paint();
        paint.setColor(Color.MAGENTA);
        paint.setStyle(Paint.Style.FILL);
        path = new Path();
        dp = context.getResources().getDimension(R.dimen.dp);
        beacon_icon = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.bl_icon), (int) (10 * dp), (int) (10 * dp), false);
        user_icon = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.kompass), (int) (10 * dp), (int) (10 * dp), false);
        imageView = new ImageView(context);
        imageView.setLayoutParams(new LayoutParams((int) (10 * dp), (int) (10 * dp)));
        imageView.setImageBitmap(user_icon);
    }

    private void init(Context context, AttributeSet attributeSet) {
        init(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        Log.d(TAG, "onMeasure: width =" + widthMeasureSpec + " ,height =" + heightMeasureSpec);
    }

    public void calculate(Activity activity, double roomW, double roomH, double cellSize, float[][] map) {
        this.map = map;

        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
        Log.d(TAG, "Dimension: height = " + height + ", width = " + width);

        one_m = (int) (width * 0.8 / roomW);
        room_width = (int) (roomW * one_m);
        room_height = (int) (roomH * one_m);
        step = room_width / map[0].length;
        startX = width / 2 - room_width / 2;
        startY = height / 2 - room_height / 2;

        findPoints();

        imageView.setX(startX - 5 * dp);
        imageView.setY(startY - 5 * dp);
    }

    public void calculatePosition() {
//        MeasurePoint[] measurePoints = new MeasurePoint[beacons.size()+accessPoints.size()];
//        for (int i = 0; i < beacons.size(); i++) {
//            measurePoints[i] = new MeasurePoint(new PositionModel(beacons.get(i).getX(), beacons.get(i).getY()), PositionHelper.getDistance(beacons.get(i).getBeacon()));
//        }
//        for (int i = 0; i < accessPoints.size(); i++) {
//            measurePoints[i] = new MeasurePoint(new PositionModel(accessPoints.get(i).getX(), accessPoints.get(i).getY()), PositionHelper.getDistance(accessPoints.get(0)));
//        }
//
//        double[] positions = PositionHelper.getPosition(measurePoints);
//        float x = (float) (startX + positions[0] * one_m);
//        float y = (float) (startY + positions[1] * one_m);
//        imageView.setX(x);
//        imageView.setY(y);
    }

    public void setBecaons(List<BeaconModel> beacons) {
        this.beacons = beacons;
    }

    private void findPoints() {
        path.moveTo(startX, startY);
        for (int i = 0; i < map.length; i++) {
            if (i < map.length - 1) {
                path.lineTo(startX + map[i + 1][0] * one_m, startY + map[i + 1][1] * one_m);
            } else {
                path.lineTo(startX, startY);
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawPath(path, paint);
        for (int i = 0; i < beacons.size(); i++) {
            canvas.drawBitmap(beacon_icon, startX + beacons.get(i).getX() * one_m, startY + beacons.get(i).getY() * one_m, new Paint());
        }
    }
}
