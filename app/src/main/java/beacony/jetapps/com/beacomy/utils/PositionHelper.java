package beacony.jetapps.com.beacomy.utils;

import com.estimote.coresdk.observation.region.RegionUtils;
import com.estimote.coresdk.recognition.packets.Beacon;
import com.lemmingapex.trilateration.NonLinearLeastSquaresSolver;
import com.lemmingapex.trilateration.TrilaterationFunction;

import org.apache.commons.math3.fitting.leastsquares.LeastSquaresOptimizer;
import org.apache.commons.math3.fitting.leastsquares.LevenbergMarquardtOptimizer;

import java.util.List;

import beacony.jetapps.com.beacomy.models.AccessPoint;
import beacony.jetapps.com.beacomy.models.MeasurePoint;

/**
 * Created by smykala-pc on 30/01/2018.
 */

public class PositionHelper {

    private static final double DISTANCE_MHZ_M = 27.55;

    public static double[] getPosition(MeasurePoint... points) {
        double[][] positions = new double[points.length][2];
        double[] distances = new double[points.length];

        for (int i = 0; i < points.length; i++) {
            MeasurePoint mp = points[i];
            positions[i][0] = mp.getPosition().getX();
            positions[i][1] = mp.getPosition().getY();
            distances[i] = mp.getDistance();
        }

        NonLinearLeastSquaresSolver solver = new NonLinearLeastSquaresSolver(new TrilaterationFunction(positions, distances), new LevenbergMarquardtOptimizer());
        LeastSquaresOptimizer.Optimum optimum = solver.solve();

        // the answer
        double[] centroid = optimum.getPoint().toArray();

        // error and geometry information; may throw SingularMatrixException depending the threshold argument provided
//        RealVector standardDeviation = optimum.getSigma(0);
//        RealMatrix covarianceMatrix = optimum.getCovariances(0);

        return centroid;
    }

    public static double getAvarageValue(List<Double> list) {
        double suma = 0;
        for (int i = 0; i < list.size(); i++)
            suma += list.get(i);
        return suma / list.size();
    }

    public static double getDistance(Beacon beacon) {
        return round(RegionUtils.computeAccuracy(beacon));
    }

    public static double getDistance(int txPower, double rssi) {
        if (rssi == 0) {
            return 0; // if we cannot determine distance, return -1.
        }

        double ratio = rssi * 1.0 / txPower;
        if (ratio < 1.0) {
            return round(Math.pow(ratio, 10));
        } else {
            double accuracy = (0.89976) * Math.pow(ratio, 7.7095) + 0.111;
            ///accuracy = round(accuracy);
            return accuracy;
        }
    }

    public static double getDistance(AccessPoint accessPoint) {
        return round(Math.pow(10.0d, ((DISTANCE_MHZ_M - (Math.log10(accessPoint.getFrequency()) * 20.0d)) + Math.abs(accessPoint.getRssi())) / 20.0d));
    }

    private static double round(double value) {
        value = (double) Math.round(value * 100.0) / 100.0;
        return value;
    }
}
