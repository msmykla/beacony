package beacony.jetapps.com.beacomy.utils;

import java.util.List;

import beacony.jetapps.com.beacomy.models.AccessPoint;

/**
 * Created by smykala-pc on 01/02/2018.
 */

public interface OnWifiListener {

    void onWifiScanned(List<AccessPoint> accessPoints);
}
