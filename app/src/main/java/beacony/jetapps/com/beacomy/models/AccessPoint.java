package beacony.jetapps.com.beacomy.models;

/**
 * Created by smykala-pc on 01/02/2018.
 */

public class AccessPoint {

    private String bssid;
    private String ssid;
    private float x;
    private float y;
    private float rssi;
    private float frequency;

    public AccessPoint() {
    }

    public AccessPoint(String bssid, String ssid, float rssi, float frequency) {
        this.bssid = bssid;
        this.rssi = rssi;
        this.ssid = ssid;
        this.frequency = frequency;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public String getBssid() {
        return bssid;
    }

    public void setBssid(String bssid) {
        this.bssid = bssid;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getRssi() {
        return rssi;
    }

    public void setRssi(float rssi) {
        this.rssi = rssi;
    }

    public float getFrequency() {
        return frequency;
    }

    public void setFrequency(float frequency) {
        this.frequency = frequency;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof AccessPoint) {
            if (((AccessPoint) obj).getBssid().equals(bssid)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Integer.parseInt(bssid);
    }
}
