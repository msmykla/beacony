package beacony.jetapps.com.beacomy.models;

/**
 * Created by smykala-pc on 30/01/2018.
 */

public class MeasurePoint {

    private PositionModel position;
    private double distance = 0;

    public MeasurePoint() {
    }

    public MeasurePoint(PositionModel position, double distance) {
        this.position = position;
        this.distance = distance;
    }

    public PositionModel getPosition() {
        return position;
    }

    public void setPosition(PositionModel position) {
        this.position = position;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

}
