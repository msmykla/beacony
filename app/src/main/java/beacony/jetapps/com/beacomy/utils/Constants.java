package beacony.jetapps.com.beacomy.utils;

import com.estimote.coresdk.observation.region.beacon.BeaconRegion;

/**
 * Created by smykala-pc on 31/01/2018.
 */

public class Constants {

    public static final BeaconRegion ALL_ESTIMOTE_BEACONS = new BeaconRegion("all Estimote beacons", null, null, null);
    public static final String UUID_BLUE = "b9407f30-f5f8-466e-aff9-25556b57fe02";
    public static final String UUID_GREEN = "b9407f30-f5f8-466e-aff9-25556b57fe03";
    public static final String UUID_BLUEBERRY = "b9407f30-f5f8-466e-aff9-25556b57fe01";
    public static final double roomWidth = 5.0;
    public static final double roomHeight = 5.0;
    public static final double cellSize = 0.5;
    public static final float[][] map = {
            {0.f, 0.f},
            {5.f, 0.f},
            {5.f, 5.f},
            {0.f, 5.f}
    };
    public static final float[][] beacons = {
            {2.5f, 0},
            {0, 2.5f},
            {5, 2.5f},
    };
}
