package beacony.jetapps.com.beacomy.utils.custom_views;

/**
 * Created by MaciekS on 2017-01-07.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import beacony.jetapps.com.beacomy.R;

public class MyCompassView extends View {

    private Paint paint;
    private Bitmap bitmap;
    private float dp;
    private double position;

    public MyCompassView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public MyCompassView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public MyCompassView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    public MyCompassView(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        bitmap = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.kompass);
        dp = context.getResources().getDimension(R.dimen.dp);
        paint = new Paint();
        Bitmap resource = bitmap;
        bitmap = Bitmap.createScaledBitmap(bitmap, (int) (25 * dp), (int) (25 * dp), false);
        if (!resource.isRecycled())
            resource.recycle();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawBitmap(bitmap, 0, 0, paint);
        //setRotation((float)position);
    }


    public void updateData(double position) {
        this.position = position;
        invalidate();
    }

}
