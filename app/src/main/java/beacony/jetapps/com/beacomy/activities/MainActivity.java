package beacony.jetapps.com.beacomy.activities;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.estimote.coresdk.observation.region.beacon.BeaconRegion;
import com.estimote.coresdk.recognition.packets.Beacon;
import com.estimote.coresdk.recognition.packets.EstimoteLocation;
import com.estimote.coresdk.service.BeaconManager;

import java.util.ArrayList;
import java.util.List;

import beacony.jetapps.com.beacomy.R;
import beacony.jetapps.com.beacomy.models.AccessPoint;
import beacony.jetapps.com.beacomy.utils.Constants;
import beacony.jetapps.com.beacomy.utils.OnWifiListener;
import beacony.jetapps.com.beacomy.utils.PositionHelper;
import beacony.jetapps.com.beacomy.utils.WifiScanner;
import beacony.jetapps.com.beacomy.utils.custom_views.MapView;

public class MainActivity extends AppCompatActivity implements SensorEventListener, BeaconManager.BeaconRangingListener, BeaconManager.LocationListener, OnWifiListener {

    public static final String TAG = MainActivity.class.getSimpleName();
    private static final String UUID_BLUE = "b9407f30-f5f8-466e-aff9-25556b57fe02";
    private static final String UUID_GREEN = "b9407f30-f5f8-466e-aff9-25556b57fe03";
    private static final String UUID_BLUEBERRY = "b9407f30-f5f8-466e-aff9-25556b57fe01";
    float[] mGravity;
    float[] mGeomagnetic;
    private BeaconManager beaconManager;
    private TextView text1, text2, text3, position;
    private SensorManager sensorManager;
    private MapView map;
    private float[] gravityData = new float[3];
    private float[] geomagneticData = new float[3];
    private boolean hasGravityData = false;
    private boolean hasGeomagneticData = false;
    private double rotationInDegrees;
    private List<Double> orientations = new ArrayList<>();
    private List<Double> distance1 = new ArrayList<>();
    private List<Double> distance2 = new ArrayList<>();
    private List<Double> distance3 = new ArrayList<>();
    private float azimut;
    private float currentDegree = 0f;
    private ImageView myCompassView;
    private boolean ranging = false;
    private WifiScanner wifiScanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);
        text1 = (TextView) findViewById(R.id.text);
        text2 = (TextView) findViewById(R.id.text2);
        text3 = (TextView) findViewById(R.id.text3);
//        position=(TextView)findViewById(R.id.position);
//        myCompassView=(ImageView) findViewById(R.id.compass);
//        map=(MapView) findViewById(R.id.map);
//        map.calculate(this);
//        map.setMyCompassView(myCompassView);
        //initScanBeacons();
        initWifiScanner();
//        region=new Region(
//                "monitored region",
//                UUID.fromString("B9407F30-F5F8-466E-AFF9-25556B57FE6D"),22504, 48827);

    }

    private void initScanBeacons() {
        beaconManager = new BeaconManager(getApplicationContext());
        beaconManager.setRangingListener(this);
        //beaconManager.setLocationListener(this);
        beaconManager.setForegroundScanPeriod(1, 0);
        beaconManager.setBackgroundScanPeriod(1, 0);
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                Log.d(TAG, "onServiceReady() called");
                beaconManager.startRanging(Constants.ALL_ESTIMOTE_BEACONS);
            }
        });
    }

    private void initWifiScanner() {
        wifiScanner = new WifiScanner(getApplicationContext(), this);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        sensorManager.registerListener(this,sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_NORMAL);
//        sensorManager.registerListener(this,sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        //SystemRequirementsChecker.checkWithDefaultDialogs(this);
        wifiScanner.register(getApplicationContext());
    }

    @Override
    protected void onPause() {
//        sensorManager.unregisterListener(this);
        super.onPause();
        wifiScanner.unregister(getApplicationContext());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        beaconManager.stopRanging(Constants.ALL_ESTIMOTE_BEACONS);
//        beaconManager.disconnect();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
//        switch (event.sensor.getType()){
//            case Sensor.TYPE_ACCELEROMETER:
//                System.arraycopy(event.values, 0, gravityData, 0, 3);
//                hasGravityData = true;
//                break;
//            case Sensor.TYPE_MAGNETIC_FIELD:
//                System.arraycopy(event.values, 0, geomagneticData, 0, 3);
//                hasGeomagneticData = true;
//                break;
//            default:
//                return;
//        }
//
//        if (hasGravityData && hasGeomagneticData) {
//            float identityMatrix[] = new float[9];
//            float rotationMatrix[] = new float[9];
//            boolean success = SensorManager.getRotationMatrix(rotationMatrix, identityMatrix,
//                    gravityData, geomagneticData);
//
//            if (success) {
//                float orientationMatrix[] = new float[3];
//                SensorManager.getOrientation(rotationMatrix, orientationMatrix);
//                float rotationInRadians = orientationMatrix[0];
//                rotationInDegrees = Math.toDegrees(rotationInRadians);
//                if(orientations.size()>=9) {
//                    orientations.add(rotationInDegrees);
//                    double ori=CalculateHelper.getAvarageValue(orientations);
//                    RotateAnimation ra = new RotateAnimation(currentDegree,(float)-ori,Animation.RELATIVE_TO_SELF, 0.5f,Animation.RELATIVE_TO_SELF, 0.5f);
//                    ra.setDuration(400);
//                    ra.setFillAfter(true);
//                    myCompassView.startAnimation(ra);
//                    currentDegree = (float)-ori;
//                    position.setText("" + ori);
//                    orientations.clear();
//                }else{
//                    orientations.add(rotationInDegrees);
//                }
//            }
//        }
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }


//    @Override
//    public void onBeaconsDiscovered(Region region, List<Beacon> list) {
//        //Log.e("Size list=",""+list.size());
//        for(int i=0;i<list.size();i++){
//            //Log.e("Beacon"+i+"=", " " + list.get(i).getRssi());
//            double distance=CalculateHelper.getDistance(list.get(i));
//            //double distance=calculateDistance(list.get(i).getMeasuredPower(),list.get(i).getRssi());//Math.pow(10,(-60.0-list.get(i).getRssi())/20.0);
//            //Log.e("UUID",""+list.get(i).getProximityUUID().toString());
//            switch (list.get(i).getProximityUUID().toString()){
//                case UUID_GREEN:
//                    distance1.add(distance);
//                    text1.setText("Distance 1: "+distance);
//                    break;
//                case UUID_BLUE:
//                    distance2.add(distance);
//                    text2.setText("Distance 2: "+distance);
//                    break;
//                case UUID_BLUEBERRY:
//                    distance3.add(distance);
//                    text3.setText("Distance 3: "+distance);
//                    break;
//            }
//        }
//        if(distance1.size()>=10 && distance2.size()>=10 && distance3.size()>=10){
//            double d1=CalculateHelper.getAvarageValue(distance1);
//            double d2=CalculateHelper.getAvarageValue(distance2);
//            double d3=CalculateHelper.getAvarageValue(distance3);
//            map.updatePosition(d1,d2,d3);
//            distance1.clear();
//            distance2.clear();
//            distance3.clear();
//        }
//    }


    @Override
    public void onBeaconsDiscovered(BeaconRegion beaconRegion, List<Beacon> beacons) {
        Log.d(TAG, "onBeaconsDiscovered() called");
        for (Beacon b : beacons) {
            //Log.d(TAG, "onBeaconsDiscovered() called with: " + b.getProximityUUID() + ", beacons = [" + b.getRssi() + ", distance =" + calculateDistance(4, b.getRssi()) + "]");
            text1.setText("rssi = " + b.getRssi() + ", TX" + b.getMeasuredPower() + ", d1 =" + PositionHelper.getDistance(b.getMeasuredPower(), b.getRssi()) + ", d2 = " + PositionHelper.getDistance(b));
        }
    }

    @Override
    public void onLocationsFound(List<EstimoteLocation> locations) {
        for (EstimoteLocation l : locations) {
            Log.d(TAG, "onLocationsFound() called with: rssi = " + l.rssi + ", txPower =" + l.txPower);
        }
    }

    @Override
    public void onWifiScanned(List<AccessPoint> accessPoints) {
        for (AccessPoint ap : accessPoints) {
            Log.d(TAG, "onWifiScanned() called : ssid = " + ap.getSsid() + ", d = " + PositionHelper.getDistance(ap));
        }
    }
}
