package beacony.jetapps.com.beacomy.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.estimote.coresdk.common.requirements.SystemRequirementsChecker;

import beacony.jetapps.com.beacomy.R;
import beacony.jetapps.com.beacomy.utils.Utils;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SystemRequirementsChecker.checkWithDefaultDialogs(this);
    }

    @OnClick(R.id.map)
    public void map() {
        boolean perm = Utils.havePermission(this);
        boolean isBluetooth = Utils.isBluetoothEnabled();
        boolean isLocation = Utils.isLocationEnabled(this);
        if (perm && isBluetooth && isLocation) {
            Intent intent = new Intent(this, MapActivity.class);
            startActivity(intent);
        } else {
            String errorText;
            if (!perm) {
                errorText = getString(R.string.need_permission);
            } else if (!isBluetooth) {
                errorText = getString(R.string.need_bluetooth);
            } else {
                errorText = getString(R.string.need_location);
            }
            Toast.makeText(this, errorText, Toast.LENGTH_SHORT).show();
            SystemRequirementsChecker.checkWithDefaultDialogs(this);
        }
    }

    @OnClick(R.id.test)
    public void test() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
