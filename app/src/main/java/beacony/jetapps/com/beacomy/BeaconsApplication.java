package beacony.jetapps.com.beacomy;

import android.app.Application;

import com.estimote.coresdk.common.config.EstimoteSDK;

/**
 * Created by smykala-pc on 23/01/2018.
 */

public class BeaconsApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        EstimoteSDK.initialize(this, getString(R.string.app_id), getString(R.string.app_token));
        EstimoteSDK.enableDebugLogging(true);
    }
}
