package beacony.jetapps.com.beacomy.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import beacony.jetapps.com.beacomy.models.AccessPoint;


/**
 * Created by smykala-pc on 01/02/2018.
 */

public class WifiScanner {

    public static final String TAG = WifiScanner.class.getSimpleName();
    private WifiManager wifiManager;
    private IntentFilter intentFilter;
    private boolean registered = false;
    private OnWifiListener onWifiListener;
    private final BroadcastReceiver networkReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onReceive() called");
            List<ScanResult> scanResults = wifiManager.getScanResults();
            Log.d(TAG, "scanResults.size() = " + scanResults.size());
            List<AccessPoint> ap = new ArrayList<>();
            for (ScanResult sr : scanResults) {
                ap.add(new AccessPoint(sr.BSSID, sr.SSID, sr.level, sr.frequency));
            }
            if (ap.size() > 0) {
                onWifiListener.onWifiScanned(ap);
            }
            if (registered) {
                wifiManager.startScan();
            }
        }
    };

    public WifiScanner() {
    }

    public WifiScanner(Context context, OnWifiListener onWifiListener) {
        init(context, onWifiListener);
    }

    public void init(Context context, OnWifiListener onWifiListener) {
        this.onWifiListener = onWifiListener;
        wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
    }

    public void register(Context context) {
        if (!registered) {
            registered = true;
            context.registerReceiver(networkReceiver, intentFilter);
            wifiManager.startScan();
        }
    }

    public void unregister(Context context) {
        if (registered) {
            registered = false;
            context.unregisterReceiver(networkReceiver);
        }
    }

}
