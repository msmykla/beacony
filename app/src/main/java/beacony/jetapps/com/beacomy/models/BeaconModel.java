package beacony.jetapps.com.beacomy.models;


import com.estimote.coresdk.recognition.packets.Beacon;

/**
 * Created by smykala-pc on 31/01/2018.
 */

public class BeaconModel {

    private String uuid;
    private float x;
    private float y;
    private Beacon beacon;

    public BeaconModel() {
    }

    public BeaconModel(String uuid, float x, float y) {
        this.uuid = uuid;
        this.x = x;
        this.y = y;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public Beacon getBeacon() {
        return beacon;
    }

    public void setBeacon(Beacon beacon) {
        this.beacon = beacon;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BeaconModel) {
            if (((BeaconModel) obj).getUuid().equals(uuid)) {
                return true;
            } else {
                return false;
            }
        } else if (obj instanceof Beacon) {
            if (((Beacon) obj).getProximityUUID().equals(uuid)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Integer.parseInt(uuid);
    }
}
